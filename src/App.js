import React, { Component } from "react";
import "./App.css";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt, faTimes } from "@fortawesome/free-solid-svg-icons";
import "bulma/css/bulma.css";

class App extends Component {
  state = {
    i1: [],
    i2: [],
    isModal: false,
    isModalEdit: false,
    changePassword: false,
    name: "",
    lname: "",
    email: "",
    password: "",
    confirmPassword: "",
    query: "",
    show: false,
  };

  updateinput = (query) => {
    // let a = i1.filter((B) => B.id == query);    used to filter using ID
    let i1 = this.state.i2;

    const filterItems = (arr, query) => {
      return arr.filter((el) => el.fullName.toLowerCase().indexOf(query.toLowerCase()) !== -1 || el.email.toLowerCase().indexOf(query.toLowerCase()) !== -1);
    };

    let names = filterItems(i1, query);
    this.setState({ i1: names, query: query });
    if (query === "") {
      this.setState({ i1: this.state.i2 });
    }
  };

  handleClick = () => {
    this.setState({ isModal: !this.state.isModal, name: "", lname: "", email: "", password: "" });
  };

  handleEditClick = (user, id) => {
    this.setState({ isModalEdit: !this.state.isModalEdit, name: user.fist_name, email: user.email, lname: user.last_name, changePassword: false, confirmPassword: "", password: "" });
  };

  onNameChange = (e) => {
    this.setState({
      name: e.target.value,
    });
  };

  onLnameChange = (e) => {
    this.setState({
      lname: e.target.value,
    });
  };

  onEmailChange = (e) => {
    this.setState({
      email: e.target.value,
    });
  };

  onPasswordChange = (e) => {
    this.setState({
      password: e.target.value,
    });
  };

  onConfirmPassword = (e) => {
    this.setState({
      confirmPassword: e.target.value,
    });
  };

  hashCode = (str) => {
    // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
  };

  intToRGB = (i) => {
    var c = (i & 0x00ffffff).toString(16).toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
  };

  handleSubmit = () => {
    const data = {
      fist_name: this.state.name,
      last_name: this.state.lname,
      email: this.state.email,
      pwd: this.state.password,
    };
    let reg = /^([A-Za-z0-9_\.])+\@([A-Za-z0-9_\.])+\.([A-Za-z]{2,4})$/;
    if (data.fist_name === "") {
      alert("Name field can't be blank");
      return false;
    } else if (data.email === "") {
      alert("Email field can't be blank");
      return false;
    } else if (data.pwd === "") {
      alert("Password field can't be blank");
      return false;
    }
    if (reg.test(data.email) === false) {
      alert("Invalid Email Address");
      return false;
    } else {
      axios
        .post("https://borosilapi.machstatz.com/add_user", data)
        .then((res) => {
          if (!alert("User Successfully added")) {
            window.location.reload();
          }
          console.log(res);
        })
        .catch((err) => console.log(err));
    }
  };

  handleEditSubmit = () => {
    const data = {
      fist_name: this.state.name,
      last_name: this.state.lname,
      email: this.state.email,
    };

    if (this.state.changePassword) {
      data["pwd"] = this.state.password;
    }
    console.log(data);
    if (data.fist_name === "") {
      alert("Name field can't be blank");
      return false;
    } else if (data.email === "") {
      alert("Email field can't be blank");
      return false;
    } else if (data.pwd === "") {
      alert("Password field can't be blank");
      return false;
    } else {
      console.log(this.state.confirmPassword + data.pwd);
      if (this.state.changePassword) {
        if (this.state.confirmPassword === data.pwd) {
          axios
            .put("https://borosilapi.machstatz.com/update_user", data)
            .then((res) => {
              if (!alert("User Successfully Updated")) {
                window.location.reload();
              }
              console.log(res);
            })
            .catch((err) => console.log(err));
        } else {
          alert("Password is not matching");
        }
      } else {
        axios
          .put("https://borosilapi.machstatz.com/update_user", data)
          .then((res) => {
            if (!alert("User Successfully Updated")) {
              window.location.reload();
            }
            console.log(res);
          })
          .catch((err) => console.log(err));
      }
    }
  };

  handleDeleteClick = (user) => {
    const data = { email: user.email };
    axios
      .delete("https://borosilapi.machstatz.com/delete_user", data)
      .then((res) => {
        alert("User was deleted Successfully");
        console.log(res);
      })
      .catch((err) => console.log(err));
  };

  componentDidMount() {
    axios
      .get("https://borosilapi.machstatz.com/get_users")
      .then((response) => {
        this.setState({ i1: response.data, i2: response.data });
        console.log(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    let data = this.state.i1;
    const active = this.state.isModal ? "is-active" : "";
    const activeEdit = this.state.isModalEdit ? "is-active" : "";
    let display = data.map((user, i) => {
      // debugger;
      const fullName = user.last_name === "" ? user.fist_name : user.fist_name + " " + user.last_name;
      user["fullName"] = fullName;
      let intial = user.fist_name.charAt(0).toUpperCase();
      let lIntial = user.last_name.charAt(0).toUpperCase();
      let imgName = intial + lIntial;
      let color = this.intToRGB(this.hashCode(user.fist_name));
      color = "#" + color;
      return (
        <div key={i} className="column1 container users">
          <div className="card">
            <div className="card-content">
              <div className="media">
                <div className="media-left">
                  <div className="iconDetails" style={{ backgroundColor: color, color: "white" }}>
                    <h1 className="centered">{imgName}</h1>
                  </div>
                </div>
                <div className="media-content">
                  <h3 className="user-name">
                    {user.fist_name} {user.last_name}
                  </h3>
                  <h3 className="user-email">{user.email}</h3>

                  <span style={{ position: "absolute", right: "15px", top: "10px", cursor: "pointer" }}>
                    <FontAwesomeIcon icon={faPencilAlt} onClick={(id) => this.handleEditClick(user, id)}></FontAwesomeIcon>
                    <FontAwesomeIcon icon={faTimes} style={{ marginLeft: "15px" }} onClick={() => this.handleDeleteClick(user)}></FontAwesomeIcon>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div className={`modal ${activeEdit}`}>
            <div className="modal-background" />
            <div className="modal-card">
              <header className="modal-card-head">
                <p className="modal-card-title">Edit User</p>
                <button onClick={this.handleEditClick} className="delete" aria-label="close" />
              </header>
              <section className="modal-card-body">
                <div className="field">
                  <label className="label">First Name</label>
                  <div className="control">
                    <input contentEditable="true" className="input" type="text" placeholder="Enter your name" name="name" value={this.state.name || ""} onChange={this.onNameChange} />
                  </div>
                </div>
                <div className="field">
                  <label className="label">Last Name</label>
                  <div className="control">
                    <input className="input" type="text" placeholder="Enter your last name" value={this.state.lname || ""} onChange={this.onLnameChange} />
                  </div>
                </div>
                <div className="field">
                  <label className="label">Email Address</label>
                  <div className="control">
                    <input className="input" type="text" placeholder="Enter your email id" value={this.state.email || ""} readOnly />
                  </div>
                </div>

                {this.state.changePassword === false ? (
                  <p className="changePassword" onClick={() => this.setState({ changePassword: true })}>
                    Change Password?
                  </p>
                ) : (
                  <div>
                    <div className="field">
                      <label className="label">Change Password</label>
                      <p className="control">
                        <input className="input" type="password" placeholder="Password" value={this.state.password} onChange={this.onPasswordChange} />
                      </p>
                    </div>
                    <div className="field">
                      <label className="label">Confirm Password</label>
                      <p className="control">
                        <input className="input" type="password" placeholder="Password" value={this.state.confirmPassword} onChange={this.onConfirmPassword} />
                      </p>
                    </div>
                  </div>
                )}
              </section>
              <footer className="modal-card-foot" style={{ height: "65px" }}>
                <div className="footerRight">
                  <button onClick={this.handleEditClick} className="button">
                    Cancel
                  </button>
                  <button className="button is-success" onClick={this.handleEditSubmit}>
                    Edit User
                  </button>
                </div>
              </footer>
            </div>
          </div>
        </div>
      );
    });

    return (
      <div className="container">
        {/* <div class="tabs">
          <ul>
            <li class="is-active">
              <a>Users</a>
            </li>
            <li>
              <a>User Managment</a>
            </li>
          </ul>
        </div> */}
        <div className="head-section">
          <input
            className="input searchbox"
            type="text"
            placeholder="Search for name or email"
            onChange={(event) => {
              this.updateinput(event.target.value);
            }}
          />

          <button onClick={this.handleClick} className="button is-link is-outlined">
            Add user <span style={{ fontSize: "25px", marginTop: "-5px", marginLeft: "5px" }}>+</span>
          </button>
        </div>
        <div className={`modal ${active}`}>
          <div className="modal-background" />
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">Add User</p>
              <button onClick={this.handleClick} className="delete" aria-label="close" />
            </header>
            <section className="modal-card-body">
              <div className="field">
                <label className="label">First Name</label>
                <div className="control">
                  <input className="input" type="text" placeholder="Enter your name" value={this.state.name || ""} onChange={this.onNameChange} required />
                </div>
              </div>
              <div className="field">
                <label className="label">Last Name</label>
                <div className="control">
                  <input className="input" type="text" placeholder="Enter your last name" value={this.state.lname || ""} onChange={this.onLnameChange} />
                </div>
              </div>
              <div className="field">
                <label className="label">Email Address</label>
                <div className="control">
                  <input className="input" type="text" placeholder="Enter your email id" value={this.state.email || ""} onChange={this.onEmailChange} required />
                </div>
              </div>
              <div className="field">
                <label className="label">Password</label>
                <p className="control">
                  <input className="input" type="password" placeholder="Password" value={this.state.password} onChange={this.onPasswordChange} required />
                </p>
              </div>
            </section>
            <footer className="modal-card-foot" style={{ height: "65px" }}>
              <div className="footerRight">
                <button onClick={this.handleClick} className="button">
                  Cancel
                </button>
                <button className="button is-success" onClick={this.handleSubmit}>
                  Add User
                </button>
              </div>
            </footer>
          </div>
        </div>

        <div className="row1">{display}</div>
      </div>
    );
  }
}

export default App;
